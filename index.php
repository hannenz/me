<!doctype html>
<html class="no-js" lang="de">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>Johannes Braun // Webentwickler Fullstack</title>
	<link rel="stylesheet" type="text/css" media="all" href="dist/css/main.css">
	<link rel="stylesheet" type="text/css" media="print" href="dist/css/printf.css">
	<link rel="shortcut icon" href="/dist/img/icons/favicon.svg">
	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "Person",
			"name": "Johannes Braun",
			"familyName": "Braun",
			"givenName": "Johannes",
			"jobTitle": "Web developer fullstack",
			"email": "me@hannenz.de",
			"image": "https://me.hannenz.de/dist/img/johannes-braun.jpg",
			"url": "https://me.hannenz.de",
			"address": {
				"@type": "PostalAddress",
				"addressLocality": "Öpfingen",
				"postalCode": "89614",
				"addressCountry": "Germany"
			}
		}
	</script>
</head>
<body id="top">

	<header class="main-header">
		<div class="main-header__top">
			<a href="#top" class="name">
				<figure hidden class="portrait">
					<picture>
						<source srcset="dist/img/johannes-braun.webp" type="image/webp">
						<source srcset="dist/img/johannes-braun.jpg" type="image/jpg">
						<img src="dist/img/johannes-braun.jpg" alt="Johannes Braun" />
					</picture>
				</figure>
				<h1>Johannes Braun <span class="slashes">//</span> <span>Webentwickler&nbsp;Fullstack</span></h1>
			</a>
			<button class="print-button screen-only" onclick="javascrip:window.print()" title="Lebenslauf drucken">
				<span>Druckversion</span>
				<?php include('dist/img/icons/print.svg'); ?>
			</button>
			<div class="color-scheme-switcher">
				<input type="checkbox" id="color-scheme-switch" name="color-scheme-switch" value="1" onchange="document.body.classList.toggle('color-scheme--dark', this.checked)">
				<label  title="Farbschema wählen (hell / dunkel)" for="color-scheme-switch">
					<?php include('dist/img/icons/sun.svg'); ?>
					<span></span>
					<?php include('dist/img/icons/moon.svg'); ?>
				</label>
			</div>
		</div>
		<nav class="main-nav">
			<button class="menu-trigger">
				<?php include('dist/img/icons/menu.svg'); ?>
			</button>
			<ul class="menu">
				<li class="menu__item"> <a class="menu__link" href="#personal">Persönliches</a> </li>
				<li class="menu__item"> <a class="menu__link" href="#skills">Skills</a> </li>
				<li class="menu__item"> <a class="menu__link" href="#occupations">Tätigkeiten</a> </li>
				<li class="menu__item"> <a class="menu__link" href="#apprenticeship">Berufsausbildung</a> </li>
				<li class="menu__item"> <a class="menu__link" href="#internship">Zivildienst & Praktika</a> </li>
				<li class="menu__item"> <a class="menu__link" href="#studies">Studium</a> </li>
				<li class="menu__item"> <a class="menu__link" href="#schooling">Schulische Ausbildung</a> </li>
				<li class="menu__item"> <a class="menu__link" href="#hobbies">Hobbys & Engagement</a> </li>
			</ul>
		</nav>
	</header>

	<figure class="portrait portrait--large portrait--screen">
		<picture>
			<source srcset="dist/img/johannes-braun.webp" type="image/webp">
			<source srcset="dist/img/johannes-braun.jpg" type="image/jpg">
			<img width="200" src="dist/img/johannes-braun.jpg" alt="Johannes Braun" />
		</picture>
	</figure>

	<main class="stack">
		 <section id="personal">
			<h2><span class="firstname">Johannes</span> <span class="lastname">Braun</span></h2>

			<div class="personal__wrapper">
				<dl>
					<dt>Geburtstag</dt>
					<dd>19.12.1977</dd>
		
					<dt>Familienstand</dt>
					<dd>verheiratet, 2 Kinder</dd>
		
					<dt>Staats&shy;ange&shy;hörigkeit</dt>
					<dd>deutsch</dd>
		
					<dt>Anschrift</dt>
					<dd>
						Gartenstra&szlig;e 13<br>
						89614 &Ouml;pfingen
					</dd>
					
					<dt>E-Mail</dt>
					<dd><a href="mailto:me@hannenz.de">me@hannenz.de</a></dd>
				</dl>

				<figure hidden class="portrait portrait--print" hidden>
					<picture>
						<source srcset="dist/img/johannes-braun.webp" type="image/webp">
						<source srcset="dist/img/johannes-braun.jpg" type="image/jpg">
						<img src="dist/img/johannes-braun.jpg" alt="Johannes Braun" />
					</picture>
				</figure>

				<figure class="qr-code" hidden>
					<?php include('dist/img/qr-me.hannenz.de.svg'); ?>
					<figcaption>Web-Version:<br>https://me.hannenz.de</figcaption>
				</figure>
			</div>
		</section>	

		<section id="skills">
			<h2>Skills</h2>

			<div class="grid">
				<dl>
					<dt>Programmier&shy;sprachen</dt>
					<dd>C/C++ <i>(sehr gut)</i>, PHP <i>(sehr gut)</i>, Javascript <i>(sehr gut)</i>, Python <i>(Grundkenntnisse)</i> <!--Vala <i>(gut)</i>, BASIC <i>(gut)</i>, 6502-Assembler <i>(sehr gut)--></i></dd>
		
					<dt>Markup und Styling</dt>
					<dd>HTML, XML, CSS, Sass</dd>
		
					<dt>Frameworks <i>(Auswahl)</i></dt>
					<dd>jQuery, NodeJs, CakePHP, Zurb Foundation</dd>
		
					<dt>Datenbanken</dt>
					<dd>MySQL / MariaDB, SQLite</dd>
		
					<dt>UNIX, GNU/Linux</dt>
					<dd>Linux-Kenntnisse; bash / zsh, tmux, GNU-Tools, vi(m), sed, awk, LAMP</dd>
		
					<dt>VCS</dt>
					<dd>Git</dd>
		
					<dt>Anwendungen <i>(Auswahl)</i></dt>
					<dd>GIMP, Inkscape, Scribus, Figma, LibreOffice</dd>
				</dl>
				<dl>
					<dt>Code</dt>
					<dd>
						<ul class="bullets icons">
							<li>
								<?php include('dist/img/icons/codeberg.svg'); ?>
								Codeberg: <a href="https://codeberg.org/hannenz">https://codeberg.org/hannenz</a></li>
							<li>
								<?php include('dist/img/icons/github.svg'); ?>
								Github: <a href="https://github.com/hannenz">https://github.com/hannenz</a></li>
							<li>
								<?php include('dist/img/icons/codepen.svg'); ?>
								Codepen: <a href="http://codepen.io/hannenz/" alt="">http://codepen.io/hannenz</a></li>
						</ul>
					</dd>

					<dt>Sprachen</dt>
					<dd>
						Deutsch <i>(Muttersprache)</i>, 
						Englisch <i>(gut)</i>, 
						Französisch <i>(Grundkenntnisse)</i>
					</dd>
				</dl>
			</div>
	
		</section>

		<section id="occupations">
			<h2>Tätigkeiten</h2>

			<dl class="three-columns">
				<div>
					<dt>seit 2012</dt>
					<dd>
						<h3>Beschäftigt als Web-Entwickler bei HALMA&thinsp;&ndash;&thinsp;Agentur für Werbung in Ulm</h3>
						<ul class="bullets">
							<li>Aufbau einer Web-Abteilung in der Werbeagentur</li>
							<li>Konzeption, Umsetzung, Anpassung, Weiterentwicklung, Wartung von Websites (Front- und Backend)
							<li>Arbeit mit verschiedenen Content Management Systemen, u.a. Typo3, Joomla, Wordpress, Content-O-Mat</li>
							<li>Mitentwicklung eines hauseigenen Content Management Systems (<a href="http://www.contentomat.de">Content-o-mat</a>)</li>
							<li>Umsetzung von HTML E-Mails, Microsites</li>
							<li>Entwicklung einer Kiosksoftware / Digital Signage</li>
							<li>Administration des Firmen-Entwicklungs-Servers (Debian/Linux)</li>
							<li>Projektmanagement</li>
							<li>Beratung</li>
						</ul>
					</dd>
				</div>
	
				<div>
					<dt>seit 2009</dt>
					<dd>
						<h3>Nebengewerbliche Tätigkeit als Web-Entwickler</h3>
						<ul class="bullets">
							<li>Umsetzung, Pflege, Anpassung und Weiterentwicklung von Websites (Front- und Backend)</li>
							<li>PHP, HTML, CSS, Javascript, individuelle PHP-Programmierungen, TYPO3</li>
						</ul>
					</dd>
				</div>
				<div>
					<dt>2007&thinsp;&ndash;&thinsp;2012</dt>
					<dd>
						<h3>Beschäftigung als Heilerziehungspfleger im Rosa-Bauer-Haus Biberach (St. Elisabeth Stiftung)</h3>
						<ul class="bullets">
							<li>Ganzheitliche Begleitung, Förderung, Betreuung und Pflege von Menschen mit geistiger und mehrfacher Behinderung auf einer Wohngruppe für Menschen mit Behinderung</li>
							<li>Mentorentätigkeit: Fachliche Anleitung und Begleitung von Auszubildenden<li>
							<li>Team- und Projektarbeit</li>
						</ul>
					</dd>
				</div>
			</dl>	
		</section>

		<section id="apprenticeship">
			<h2>Berufsausbildung</h2>

			<dl>
				<dt>2008&thinsp;&ndash;&thinsp;2009</dt>
				<dd>
					<h3>Fernlehrgang: C/C++ Programmierer unter Linux</h3>
					<p>Fachgebiete: Betriebssystem UNIX, Programmiersprache C unter Linux, Programmiersprache C++ unter Linux</p>
					<p class="achievement">Abschluss: geprüfter C/C++ - Programmierer unter Linux (SGD)</p>
				</dd>			
				<dt>2005&thinsp;&ndash;&thinsp;2007</dt>
				<dd>
					<h3>Berufsausbildung zum Heilerziehungspfleger</h3>
					<p>Institut für soziale Berufe Bad Wurzach</p>
					<p class="achievement">Abschluss: Staatlich geprüfter Heilerziehungspfleger</p>
				</dd>
			</dl>
		</section>

		<section id="internship">
			<h2>Zivildienst&thinsp;&amp;&thinsp;Praktika</h2>

			<dl>
				<dt>2004&thinsp;&ndash;&thinsp;2005</dt>
				<dd>
					<h3>Praktikum</h3>
					<p>Heggbacher Einrichtungen: Schule St. Franziskus für Kinder und Jugendliche mit geistigen oder mehrfachen Behinderungen, Vorpraktikum zur Ausbildung zum Heilerziehungspfleger</p>
				</dd>

				<dt>1998&thinsp;&ndash;&thinsp;2000</dt>
				<dd>
					<h3>Zivildienst und Praktikum</h3>
					<p>Heggbacher Einrichtungen, Wohnheim St. Maria Ingerkingen (Behindertenhilfe)</p>
				</dd>
			</dl>
		</section>

		<section id="studies">
			<h2>Studium</h2>

			<dl>
				<dt>2003</dt>
				<dd>
					<h3>Studium an der Universität Tübingen: Informatik</h3>
					</p>
					<p class="achievement">ohne Abschluß</p>
				</dd>

				<dt>2000&thinsp;&ndash;&thinsp;2003</dt>
				<dd>
					<h3>Studium an der Universität Tübingen: Englisch und Biologie (Lehramt)</h3>
					</p>
					<p class="achievement">ohne Abschluß</p>
				</dd>
			</dl>
		</section>

		<section id="schooling">
			<h2>Schulische Ausbildung</h2>

			<dl>
				<dt>1988&thinsp;&ndash;&thinsp;1998</dt>
				<dd>
					<h3>Gymnasium Ehingen</h3>
					<p class="achievement">Allgemeine Hochschulreife</p>
				</dd>

				<dt>1984&thinsp;&ndash;&thinsp;1988</dt>
				<dd>
					<h3>Michel-Buck-Schule Ehingen</h3>
					<p>Grundschule</p>
				</dd>
			</dl>
		</section>

		<section id="hobbies">
			<h2>Hobbys &amp; Engagement</h2>
	
			<dl>
				<dt>Interessen</dt>
				<dd>
					<ul class="bullets">
						<li>Musik (Gitarre, Schlagzeug), Band, Chor</li>
						<li>Zeichnen, Gestalten, Malen</li>
						<li>Laufen</li>
						<li>Programmieren auf »historischen« 8-Bit-Computern (z.&nbsp;Bsp.&nbsp;C-64)</li>
					</ul>
				</dd>
				<dt>Engagement</dt>
				<dd>
					bis ca. 2004 in der Jugendarbeit: 
					<ul class="bullets">
						<li>Jugendhaus Ehingen (Vorstandschaft im SJR Ehingen)</li>
						<li><a href="https://www.zeltlager-rammetshofen.de">BDKJ Zeltlager Rammetshofen</a>: Betreuer und Lagerleitung<br></li>
					</ul>
				</dd>
			</dl>
		</section>
	</main>

	<footer class="main-footer">
		<p>2023 Johannes Braun &middot; me@hannenz.de &middot; <a href="https://www.hannenz.de">https://www.hannenz.de</a></p>
	</footer>

	<script src="dist/js/main.js"></script>
</body>
</html>
