class Main {

	constructor() {
		document.addEventListener('DOMContentLoaded', this.init);
	}

	init() {
		document.documentElement.classList.remove('no-js');

		let p = window.matchMedia('(prefers-color-scheme: dark)')
		const sw = document.getElementById('color-scheme-switch');
		document.body.classList.toggle('color-scheme--dark', p.matches);
		sw.toggleAttribute('checked', p.matches);

		const menuItems = document.querySelectorAll('.menu__link');

		const sections = document.querySelectorAll('section');
		const options = {
			// root: document.body,
			rootMargin: '0px',
			threshold: [0.05, 0.5]
		};
		let observer = new IntersectionObserver((entries) => {
			entries.map((entry) => {
				entry.target.classList.toggle('in-view', entry.isIntersecting);

				entry.target.classList.toggle('is-current', entry.intersectionRatio >= 0.5);
				if (entry.intersectionRatio >= 0.5) {
					menuItems.forEach((item) => {
						item.classList.toggle('is-current', item.getAttribute('href') == `#${entry.target.id}`);
					});
				}
			});

		}, options);

		sections.forEach((section) => {
			observer.observe(section);
		});



		let observer2 = new IntersectionObserver((entries) => {
			entries.map((entry) => {
				document.body.classList.toggle('has-scrolled', !entry.isIntersecting);
			});
		}, { rootMargin: '-100px 0px 0px 0px' });
		let portrait = document.querySelector('.portrait--large');
		observer2.observe(portrait);

		let menuBtn = document.querySelector('.menu-trigger');
		menuBtn.addEventListener('click', e => {
			document.body.classList.toggle('menu-is-open');
		});



		document.body.addEventListener('keyup', e => {
			if (e.keyCode == 27) {
				document.body.classList.remove('menu-is-open');
			}
		});
		document.body.addEventListener('click', e => {
			if (!e.target.closest('.menu-trigger')) {
				document.body.classList.remove('menu-is-open');
			}
		});

	}
}

new Main();
